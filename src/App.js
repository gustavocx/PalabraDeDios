import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Header from './component/Header';
import Home from './component/home';
import Footer from './component/Footer';

/* IMPORT CREENCIAS */
import Creencia1 from './component/28Creencias/creencia1';
/* import Creencia2 from '../component/28Creencias/creencia2';
import Creencia3 from '../component/28Creencias/creencia3';
import Creencia4 from '../component/28Creencias/creencia4';
import Creencia5 from '../component/28Creencias/creencia5';
import Creencia6 from '../component/28Creencias/creencia6';
import Creencia7 from '../component/28Creencias/creencia7';
import Creencia8 from '../component/28Creencias/creencia8';
import Creencia9 from '../component/28Creencias/creencia9';
import Creencia10 from '../component/28Creencias/creencia10';
import Creencia11 from '../component/28Creencias/creencia11';
import Creencia12 from '../component/28Creencias/creencia12';
import Creencia13 from '../component/28Creencias/creencia13';
import Creencia14 from '../component/28Creencias/creencia14';
import Creencia15 from '../component/28Creencias/creencia15';
import Creencia16 from '../component/28Creencias/creencia16';
import Creencia17 from '../component/28Creencias/creencia17';
import Creencia18 from '../component/28Creencias/creencia18';
import Creencia19 from '../component/28Creencias/creencia19';
import Creencia20 from '../component/28Creencias/creencia20';
import Creencia21 from '../component/28Creencias/creencia21';
import Creencia22 from '../component/28Creencias/creencia22';
import Creencia23 from '../component/28Creencias/creencia23';
import Creencia24 from '../component/28Creencias/creencia24';
import Creencia25 from '../component/28Creencias/creencia25';
import Creencia26 from '../component/28Creencias/creencia26';
import Creencia27 from '../component/28Creencias/creencia27';
import Creencia28 from '../component/28Creencias/creencia28'; */
/*/IMPORT CREENCIAS */

/* IMPORT MANDAMIENTOS DE DIOS */
import MandamientosDeDios from './component/MandamientosDeDios';
import NullPath from './component/nullPath';

function App() {
  return (
    <>

      <Router>

        <Header />

        <Switch>

          <Route exact path="/">
            <Home />
          </Route>

          {/* ROUTE CREENCIAS */}
          <Route exact path="/PalabraDeDios">
            <Creencia1 />
          </Route>

          {/*/ROUTE CREENCIAS */}

          {/* ROUTE Mandamientos De Dios */}
          <Route exact path="/MandamientosDeDios">
            <MandamientosDeDios />
          </Route>
          {/*/ROUTE Mandamientos De Dios */}

          {/* URL OMNILIFE */}
          <Route exact path="/url/omnilife" render={() => (window.location = "https://portal.omnilife.com/registro?distributor_code=060188707CAG")} />
          <Route exact path="/url/client-omnilife" render={() => (window.location = "https://portal.omnilife.com/registro-cliente?distributor_code=060188707CAG")} />
          <Route exact path="/url/seytu" render={() => (window.location = "https://seytu.omnilife.com/registro?distributor_code=060188707CAG")} />
          <Route exact path="/url/client-seytu" render={() => (window.location = "https://seytu.omnilife.com/registro-cliente?distributor_code=060188707CAG")} />
          {/*/URL OMNILIFE */}

          <Route path="*">
            <NullPath />
          </Route>

        </Switch>


      </Router>

      <Footer />

    </>
  );
}

export default App;