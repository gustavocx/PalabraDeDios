import React from "react";
import { Link } from "react-router-dom";
import Creencias from './28Creencias/creencias';

function Header() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">

            <Link to="/" className="navbar-brand">Palabra de Dios</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav">

                    <Creencias />

                    <li className="nav-item">
                        <Link to="/MandamientosDeDios" className="nav-link">Mandamientos de Dios</Link>
                    </li>

                </ul>
            </div>

        </nav>
    );
}

export default Header;