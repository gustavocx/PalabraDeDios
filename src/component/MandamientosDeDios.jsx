function MandamientosDeDios() {
    return (
        <div className="container p-3 bg-white colorBlack">

            <h1>Los Mandamientos De Dios</h1>

            <p>1. “No tendrás otros dioses delante de mí.</p>

            <p>2. “No te harás imagen, ni ninguna semejanza de lo que esté arriba en el cielo ni abajo en la tierra ni en las aguas debajo de la tierra. No te inclinarás ante ellas ni les rendirás culto, porque yo soy el SEÑOR tu Dios, un Dios celoso que castigo la maldad de los padres sobre los hijos, sobre la tercera y sobre la cuarta generación de los que me aborrecen. Pero muestro misericordia por mil generaciones a los que me aman y guardan mis mandamientos.</p>

            <p>3. “No tomarás en vano el nombre del SEÑOR tu Dios, porque el SEÑOR no dará por inocente al que tome su nombre en vano.</p>

            <p>4. “Acuérdate del día sábado para santificarlo. Seis días trabajarás y harás toda tu obra, pero el séptimo día será sábado para el SEÑOR tu Dios. No harás en él obra alguna, ni tú, ni tu hijo, ni tu hija, ni tu esclavo, ni tu esclava, ni tu animal, ni el forastero que está dentro de tus puertas. Porque en seis días el SEÑOR hizo los cielos, la tierra y el mar, y todo lo que hay en ellos, y reposó en el séptimo día. Por eso el SEÑOR bendijo el día sábado y lo santificó.</p>

            <p>5. “Honra a tu padre y a tu madre, para que tus días se prolonguen sobre la tierra que el SEÑOR tu Dios te da.</p>
            <p>6. “No cometerás homicidio.</p>

            <p>7. “No cometerás adulterio.</p>

            <p>8. “No robarás.</p>

            <p>9. “No darás falso testimonio contra tu prójimo.</p>

            <p>10. “No codiciarás la casa de tu prójimo; no codiciarás la mujer de tu prójimo, ni su esclavo, ni su esclava, ni su buey, ni su asno, ni cosa alguna que sea de tu prójimo”.</p>

            <h1>Éxodo 20:3-17 / Deuteronomio 5:7-21</h1>

        </div>
    );
}

export default MandamientosDeDios;