import React from 'react';

function ComoEstudiarLaBiblia() {
    return (
        <div>
            
            <h1>Cómo Estudiar La Biblia</h1><br/>

            <p>Juan 5:39-40:</p>
            <p>Escudriñad las Escrituras, porque os parece que en ellas tenéis vida eterna, y ellas son las que dan testimonio de mí.</p>
            <p>Y vosotros no queréis venir a mí para que tengáis vida.</p>

            <p>Salmos 25:14</p>
            <p>El secreto de Jehovah es para los que le temen; a ellos hará conocer su pacto.</p>

            <p>Mateo 6:33</p>
            <p>Más bien, buscad primeramente el reino de Dios y su justicia, y todas estas cosas os serán añadidas. </p>

            <p>Jeremías 33:3</p>
            <p>‘Clama a mí, y te responderé; y te revelaré cosas grandes e inaccesibles que tú no conoces.</p>

            <p>Salmos 37:7</p>
            <p>Calla delante de Jehovah, y espera en él. No te alteres con motivo de los que prosperan en su camino, por el hombre que hace maldades.</p>

            <p>Isaías 30:21</p>
            <p>Entonces tus oídos oirán a tus espaldas estas palabras:“¡Este es el camino; andad por él, ya sea que vayáis a la derecha o a la izquierda!”.</p>

            <p>1 Juan 4:13-16</p>
            <p>En esto sabemos que permanecemos en él y él en nosotros:en que nos ha dado de su Espíritu.</p>
            <p>Y nosotros hemos visto y testificamos que el Padre ha enviado al Hijo como Salvador del mundo.</p>
            <p>El que confiesa que Jesús es el Hijo de Dios, Dios permanece en él, y él en Dios.</p>
            <p>Y nosotros hemos conocido y creído el amor que Dios tiene para con nosotros. Dios es amor. Y el que permanece en el amor permanece en Dios, y Dios permanece en él.</p>

            <p>Proverbios 4:18</p>
            <p>Pero la senda de los justos es como la luz de la aurora que va en aumento hasta que es pleno día.</p>

            <p>Lucas 11:28</p>
            <p>Y él dijo:—Más bien, bienaventurados son los que oyen la palabra de Dios y la guardan.</p>

            <p>Daniel 10:3</p>
            <p> No comí manjares delicados ni carne ni vino entraron en mi boca ni me ungí con aceite, hasta que se cumplieron tres semanas.</p>

            <p>Jeremías 15:16</p>
            <p>Fueron halladas tus palabras, y yo las comí. Tus palabras fueron para mí el gozo y la alegría de mi corazón; porque yo soy llamado por tu nombre, oh Jehovah Dios de los Ejércitos.</p>

            <p>Juan 6:63</p>
            <p>El Espíritu es el que da vida; la carne no aprovecha para nada. Las palabras que yo os he hablado son espíritu y son vida.</p>

            <p>Mateo 7:24-27</p>
            <p>“Cualquiera, pues, que me oye estas palabras y las hace, será semejante a un hombre prudente que edificó su casa sobre la roca.</p>
            <p>Y cayó la lluvia, vinieron torrentes, soplaron vientos y golpearon contra aquella casa. Pero ella no se derrumbó, porque se había fundado sobre la roca.</p>
            <p>“Pero todo el que me oye estas palabras y no las hace, será semejante a un hombre insensato que edificó su casa sobre la arena.</p>
            <p>Cayó la lluvia, vinieron torrentes, y soplaron vientos, y azotaron contra aquella casa. Y se derrumbó, y fue grande su ruina.” </p>

        </div>

    );
}

export default ComoEstudiarLaBiblia;
