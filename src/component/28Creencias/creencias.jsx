import React from "react";
import { Link } from "react-router-dom";

function Creencias() {
    return (
        <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          28 Creencias
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">

          <Link to="/PalabraDeDios" className="dropdown-item">Palabra de Dios</Link>
          {/* <a className="dropdown-item" href="#LaDeidad">La Deidad</a>
          <a className="dropdown-item" href="#DiosElPadre">Dios el Padre</a>
          <a className="dropdown-item" href="#DiosElHijo">Dios el Hijo</a>
          <a className="dropdown-item" href="#DiosElEspirituSanto">Dios el Espíritu Santo</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#LaCreacion">La creación</a>
          <a className="dropdown-item" href="#LaNaturalezaHumana">La naturaleza humana</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#ElGranConflicto">El gran conflicto</a>
          <a className="dropdown-item" href="#LaVidaMuerteYResurreccionDeCristo">La vida, muerte y resurrección de Cristo</a>
          <a className="dropdown-item" href="#LaExperienciaDeLaSalvacion">La experiencia de la salvación</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#CrecerEnCristo">Crecer en Cristo</a>
          <a className="dropdown-item" href="#LaIglesia">La iglesia</a>
          <a className="dropdown-item" href="#ElRemanenteYSuMision">El remanente y su misión</a>
          <a className="dropdown-item" href="#LaUnidadEnElCuerpoDeCristo">La unidad en el cuerpo de Cristo</a>
          <a className="dropdown-item" href="#ElBautismo">El bautismo</a>
          <a className="dropdown-item" href="#LaCenaDelSenor">La cena del Señor</a>
          <a className="dropdown-item" href="#LosDonesYMinisteriosEspirituales">Los dones y ministerios espirituales</a>
          <a className="dropdown-item" href="#ElDonDeProfecia">El don de profecía</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#LaLeyDeDios">La ley de Dios</a>
          <a className="dropdown-item" href="#ElSabado">El sábado</a>
          <a className="dropdown-item" href="#LaMayordomia">La mayordomía</a>
          <a className="dropdown-item" href="#LaConductaCristiana">La conducta cristiana</a>
          <a className="dropdown-item" href="#ElMatrimonioYLaFamilia">El matrimonio y la familia</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#ElMinisterioDeCristoEnElSantuarioCelestial">El ministerio de Cristo en el Santuario celestial</a>
          <a className="dropdown-item" href="#LaSegundaVenidaDeCristo">La segunda venida de Cristo</a>
          <a className="dropdown-item" href="#LaMuerteYLaResurreccion">La muete y la resurreción</a>
          <a className="dropdown-item" href="#ElMilenioYElFinDelPecado">El milenio y el fin del pecado</a>
          <a className="dropdown-item" href="#LaTierraNueva">La Tierra Nueva</a> */}
        
        </div>
      </li>
    );
}

export default Creencias;