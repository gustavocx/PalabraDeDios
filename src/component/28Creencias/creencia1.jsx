import closedBible from '../../svg/closedBible.svg'

function Creencia1() {
  return (
    <div className="container p-3 bg-white colorBlack">
      <h1 className="text-center">La Palabra de Dios</h1>
      <p>Las Sagradas Escrituras, que abarcan el Antiguo y el Nuevo Testamento, constituyen la Palabra de Dios escrita, transmitida por inspiración divina mediante santos hombres de Dios que hablaron y escribieron impulsados por el Espíritu Santo. Por medio de esta Palabra, Dios ha comunicado a los seres humanos el conocimiento necesario para alcazar la salvación. Las Sagradas Escrituras son la infalible revelación de la voluntad divina. Son la norma del carácter, el criterio para evaluar la experiencia, la revelación autorizada de las doctrinas, un registro fidedigno de los actos de Dios realizados en el curso de la historia.</p>
      <br/>
      <img className="mx-auto d-block" height="250" src={closedBible} draggable="false" alt="Bible"></img>

    </div>
  );
}

export default Creencia1;